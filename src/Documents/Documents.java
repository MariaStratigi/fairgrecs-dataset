/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Documents;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author user
 */
public class Documents {

    
    private List<String> subTree;
    private Set<String> keys;
    private Set<String> stopwords;
    private List<String> keywords;
    private int numOfKeywords;
    
    private Map<Integer, Set<String>> docs;
    private Set<String> codes;
    private final String nodeName;
    private int numOfDocs;
    private int startId;
    private int endId;
    
    public Documents(String name) {
        nodeName = name;
        keys = new HashSet<>();
        subTree = new ArrayList<>();
        keywords = new ArrayList<>();
        docs = new HashMap<>();
    }
    
    public Documents(String name, Set<String> desc, Set<String> stop){
        this.nodeName = name;
        this.keys = desc;
        this.stopwords = stop;
        keywords = new ArrayList<>();
        docs = new HashMap<>();
        generateKeywords();
    }

    public void addNameToSubTree(String name) {
        subTree.add(name);
    }

    public String getNodeName() {
        return nodeName;
    }


    public Set<String> getKeys() {
        return keys;
    }

    public void setStopwords(Set<String> stop) {
        stopwords = stop;
    }

    public void printCodes(){
        for(String s : codes){
            System.out.println(s);
        }
    }

    public void setNumOfKeywords(int numOfKeywords) {
        this.numOfKeywords = numOfKeywords;
    }
    
    public int createDocs(int num, int id, int numRated){
        this.numOfDocs = num;
        int min = 0;
        int max = keywords.size();
        int randomNum;
        
        this.startId = id;
        this.endId = startId + numRated;
        if(keywords.size() <= this.numOfKeywords){
            
            Set<String> kw = new HashSet<>();
            kw.addAll(keywords);
            for(int i=0; i<num; i++){
                docs.put(id, kw);
                id++;
            }
            return id;
        }
        for(int i=0; i<num; i++){
            Set<String> kw = new HashSet<>();
            while(kw.size() <= this.numOfKeywords){
                randomNum = ThreadLocalRandom.current().nextInt(min, max);
                String tmp = keywords.get(randomNum);
                kw.add(tmp);
            }
            docs.put(id, kw);
            id++;
        }
        return id;
    }

    public Map<Integer, Set<String>> getDocs() {
        return docs;
    }

    public Set<String> getCodes() {
        return codes;
    }

    public int getNumOfDocs() {
        return numOfDocs;
    }
    
    private void generateKeywords(){
        for(String s : keys){
            String [] part = s.split(" ");
            for(String p : part){
                String tmp = p.replaceAll("[^a-zA-Z ]", "");
                tmp = tmp.trim();
                if(tmp.equalsIgnoreCase("")){
                    continue;
                }
                if(stopwords.contains(tmp)){
                    continue;
                }
                if(!keywords.contains(tmp)){
                    keywords.add(tmp);
                }
            }
        }
    }

    public int getStartId() {
        return startId;
    }

    public int getEndId() {
        return endId;
    }
    
    public String toString(){
        String str = "";
        
        str = this.nodeName+"\t[";
        
        for(int id : docs.keySet()){
            System.out.print(id + "\t[");
            Set<String> set = docs.get(id);
            for(String s : set){
                System.out.print(" " + s);
            }
            System.out.print("]");
            System.out.println();
        }
        return str;
    }
}
