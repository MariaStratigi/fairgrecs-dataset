/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DatasetCreation;

import Documents.BuildDocuments;
import Documents.Documents;
import Documents.XML_Parser;
import Patients.CreatePatient;
import Variables.Variables;
import java.io.File;
import java.util.List;
import ratings.GenerateSyntheticRatings;

/**
 *
 * @author mariastr
 */
public class DatasetCreation {
    
    Variables var;
    
    public DatasetCreation(Variables var){
        this.var = var;
    }
    
    public void createDataset(){
        
        /**
         * Documents
         */
       
        String output = var.getOutput();
        if(!output.endsWith("\\")){
            output = output + "\\";
        }

        XML_Parser xml = new XML_Parser();

        // Full path of the ICD-10 ontology xml file 'ICD-10.xml'
        //File icd10 = new File(var.getOntology());
        //String icd10Path = icd10.getAbsolutePath();
        xml.parseXMLFile(var.getOntology());

        System.out.println("Creating Documents ...");
        BuildDocuments bd = new BuildDocuments(xml,var.getStopwords());
        bd.setNumRated(var.getNumRated());
        bd.buildDocuments(var.getNumKeywords(), var.getNumDocs());
        List<Documents> docs = bd.getNodes();
        bd.printDocuments(output);

        /**
         * Patients
         */
        System.out.println("Creating Patients ...");
        //Full path of the 'PatientCorePopulatedTable.txt' file
//        File patient = new File(var.getPatients());
//        String patientPath = patient.getAbsolutePath();
        CreatePatient cp = new CreatePatient(var.getPatients());
        //Full path of the 'AdmissionsDiagnosesCorePopulatedTable.txt' file
//        File admission = new File(var.getAdmissions());
//        String admissionPath = admission.getAbsolutePath();
        cp.ReadIllnessFile(var.getAdmissions());

        //The percentages for the three different groups
        double gA = var.getGroupSparse() / 100.0;
        double gB = var.getGroupRegular() / 100.0;
        double gC = var.getGroupDedicated() / 100.0;
        double groupAper = gA;
        double groupBper = gB;
        double groupCper = gC;

        //The ranges from which a randomly number will be choosen as a patient's total number of ratings, according to his/her group
        
        int rangeAmin = var.getSparseMin();
        int rangeAmax = var.getSparseMax();

        int rangeBmin = var.getRegularMin();
        int rangeBmax = var.getRegularMax();

        int rangeCmin = var.getDedicatedMin();
        int rangeCmax = var.getDedicatedMax();

        cp.divideGroups(groupAper, rangeAmin, rangeAmax, groupBper, rangeBmin, rangeBmax, groupCper, rangeCmin, rangeCmax);

        //The percentages of the number of ratings that a user will give which will be relevant with his/her health problems
        double hr = var.getHealthRelevant() / 100.0;
        double nhr = var.getNoHealthRelevant() / 100.0;
        double healthRelevant = hr;
        double noRelevant = nhr;

        cp.setRatingsSpan(healthRelevant, noRelevant);
        int totalRatings = cp.getTotalRatings();
        /**
         * Ratings
         */
        System.out.println("Creating Ratings Data...");
        double tail = 0.5;
        GenerateSyntheticRatings rat = new GenerateSyntheticRatings(cp.getPatients(), docs,  bd.getCategoryLex());
        System.out.println("Finalize Rating Data");
        rat.setNumRating(totalRatings);

        //The percentages of the values of the ratings
        //double one = 0.2, two = 0.1, three = 0.3, four = 0.2, five = 0.2;
        double one = var.getScore1() / 100.0;
        double two = var.getScore2() / 100.0;
        double three = var.getScore3() / 100.0;
        double four = var.getScore4() / 100.0;
        double five = var.getScore5() / 100.0;
        rat.generateRating(one, two, three, four, five);
        rat.printRatings(output);
    }
}
