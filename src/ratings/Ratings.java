/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ratings;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author user
 */
public class Ratings {

    private final int min, max;
    private Map<Integer, Integer> ratings;
    private List<Integer> ratedDocs;
    private int totalRatings;
    private int filledRatings;
    private int numOfRatings;
    private int inCat, outCat;

    public Ratings(String group, int rmin, int rmax) {

        min = rmin;
        max = rmax;
        numOfRatings = ThreadLocalRandom.current().nextInt(min, max);

        totalRatings = 0;
        filledRatings = 0;
        ratings = new HashMap<>();
        ratedDocs = new ArrayList<>();
    }

    public boolean addDocumentForRating(int id) {
        if (ratedDocs.contains(id)) {
            return false;
        }
        ratedDocs.add(id);
        return true;
    }

    public void AddToTotalRatings() {
        totalRatings++;
    }

    public int getMin() {
        return min;
    }

    public int getMax() {
        return max;
    }

    public Map<Integer, Integer> getRatings() {
        return ratings;
    }

    public int getTotalRatings() {
        return totalRatings;
    }

    public boolean hasMoreRatings() {
        if (this.totalRatings < numOfRatings) {
            return true;
        }
        return false;
    }

    public boolean isDocumentRated(int id) {
        return ratings.containsKey(id);
    }

    public boolean hasToFillRatings() {
        if (this.filledRatings < this.totalRatings) {
            return true;
        }
        return false;
    }

    public boolean fillRating(int num) {
        int min = 0;
        int max = this.ratedDocs.size();
        while (true) {
            int randomNum = ThreadLocalRandom.current().nextInt(min, max);
            int docId = this.ratedDocs.get(randomNum);
            if (ratings.containsKey(docId)) {
                continue;
            }
            ratings.put(docId, num);
            filledRatings++;
            return true;
        }
    }

    public int getFilledRatings() {
        return filledRatings;
    }

    public int getNumOfRatings() {
        return numOfRatings;
    }

    public void setCategoriesSpan(int in, int out) {
        this.inCat = in;
        this.outCat = out;
    }

    public boolean canAddRatingIn() {
        if (inCat > 0) {
            return true;
        }
        return false;
    }

    public boolean canAddRatingOut() {
        if (outCat > 0) {
            return true;
        }
        return false;
    }

    public void adjustInCounter() {
        this.inCat = inCat - 1;
    }

    public void adjustOutCounter() {
        this.outCat = outCat - 1;
    }
}
